const express = require("express");
const app = express();

// Set View Engine
app.set('view engine', 'ejs');

app.get('/home', (req, res) => {
    res.render('home', {title: 'Halaman Home'})
})

app.get('/login', (req, res) => {
    res.render('login', {
        title: 'Halaman Login',
        desc: 'Selamat kamu berhasil login'
    })
})

app.get('/register', (req, res) => {
    res.render('register', {
        title: 'Halaman Register',
        desc: 'Selamat pendaftaran anda berhasil'
    })
})

app.get('/shoppingcart', (req, res) => {
    res.render('shoppingcart', {title: 'Halaman Shopping Cart'})
})

app.get('/category', (req, res) => {
    res.render('category', {
        title: 'Halaman Category',
        desc: 'Berikut Kategori Buku yang Tersedia:'})
})

app.get('/detailBook', (req, res) => {
    res.render('detailBook', {
        title: 'Halaman Detail Book',
        bookTitle: 'Perahu Kertas',
        desc : 'Buku ini dapat dibeli di Toko buku kesayangan anda!'
    })
})

app.listen(8000, () => {
    console.log("Server running on port 8000.")
})